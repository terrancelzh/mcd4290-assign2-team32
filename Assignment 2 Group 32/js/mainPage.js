// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem("selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}

let runsListElement = document.getElementById("runsList");
console.log(runsListElement);

retrieveFromLocal = JSON.parse(localStorage.getItem("runKey")); //Retrieve objects from local storage
let nameArray = [];
console.log(retrieveFromLocal)

if (retrieveFromLocal !== null)
{
    for (i = 0; i < retrieveFromLocal.length; i++)
        {
            nameArray.push(retrieveFromLocal[i]._runName);
        }
    console.log(nameArray);
    let listHTML = "";  // List view section heading
    // HTML format of list item 
    for (var i = 0; i<retrieveFromLocal.length; i++){
    listHTML += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun("+i+");\">";
    listHTML += "<span class=\"mdl-list__item-primary-content\">"
    listHTML += "<span>" + retrieveFromLocal[i]._runName + "</span>"
    listHTML += "<span class=\"mdl-list__item-sub-title\">" + "Date: " + retrieveFromLocal[i]._startTime + "</span>"
    listHTML += "</span>"
    listHTML += "</li>"
    
    //Insert the list view elements into the run list
    runsListElement.innerHTML = listHTML;
    }
}
    