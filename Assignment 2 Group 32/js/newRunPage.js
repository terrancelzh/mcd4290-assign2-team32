// Code for the Measure Run page.
//Define variables
var longitude, latitude, accuracy, lng1, lat1, lng2, lat2, distance, accuracy, currentLoc, startLoc, newMarker, newRand , endLo, intervalLoc, startTime, endTime, distanceLeft, timeTaken, realTimeDistance, runInstance, storedRun, runName;
//Empty array to place the locations
var immediateLocations = []; 

//Mapbox access key
mapboxgl.accessToken = 'pk.eyJ1IjoidGVycmFuY2UwMCIsImEiOiJjazg4N3hiNGcwMG80M21teno1OXZseWxyIn0.9wSAxC4E13m9js90fUkBZg';

//Display map
let map = new mapboxgl.Map({
    container: 'map', //container id
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [-96,37.8], //starting position
    zoom: 16 //starting zoom
});              

// GPS sensor code (geolocation)
function displayElementsWithClass(className, display){
    var elements = document.getElementsByClassName(className);

    for (var i = 0; i < elements.length; i++){
        if (display){
                elements[i].style.display = 'block';
        }
        else{
                elements[i].style.display = 'none';
        }
    }
}

if (navigator.geolocation){
    positionOptions = {
        enableHighAccuracy: true,
        timeout: Infinity,
        maximumAge: 0
    };
    displayElementsWithClass('gpsError', false);
    navigator.geolocation.watchPosition(showCurrentLocation, errorHandler, positionOptions);
}
else{
    displayElementsWithClass('gpsValue', false);
}

function errorHandler(error){
    if (error.code == 1){
        alert('Location access denied by user.');
    }
    else if (error.code == 2){
        alert('Location unavailable.');
    }
    else if (error.code == 3){
        alert('Location access timed out');
    }
    else{
        alert('Unknown error getting location.');
    }
}                  

//Show current location
function showCurrentLocation(position){
    longitude = Number(position.coords.longitude).toFixed(4); //Longitude
    latitude = Number(position.coords.latitude).toFixed(4); //Latitude
    accuracy = Number(position.coords.accuracy).toFixed(2); //Accuracy 
    currentLoc = [longitude, latitude];
    //Add popup the current location
    var popup = new mapboxgl.Popup({ offset: 25 }).setText('You are here!');
    //Add marker to the current location
    var marker = new mapboxgl.Marker()
        .setLngLat(currentLoc)
        .setPopup(popup)
        .addTo(map);
    //Pan to the current location
    map.panTo(currentLoc);
    //Accuracy check 
    if (accuracy <= 20){ 
    document.getElementById('randomDestination').disabled = false;
    };
    //10m from Destination check
    if (realTimeDistance <= 100){
        endRun();
    }
}

// From degree to radian
function deg2rad(deg){
    return deg * (Math.PI/180);
}

//Apply Haversine formula to calculate the distance
function distcheck(lng1, lat1, lng2, lat2){
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLng = deg2rad(lng2-lng1); 
    var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLng/2) * Math.sin(dLng/2)
        ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d * 1000;
}

//Generates a random integer
function getRandomInt(max){ 
  return Math.floor(Math.random() * Math.floor(max));
}

//Generates a random longitude
function generateRandomLong(){
    var num1 = getRandomInt(1000)/1000000;
    var posOrNeg = Math.round(Math.random());
    if (posOrNeg == 0) {
        num1 = num1 * -1;
    }
    return num1;
}

//Generates a random latitude
function generateRandomLat(){
    var num2 = getRandomInt(1000)/1000000;
    var posOrNeg = Math.round(Math.random());
    if (posOrNeg == 0) {
        num2 = num2 * -1;
    }
		return num2;
}

//Generates the randomDestination
function randomDestination(){
	lng1 = currentLoc[0];
	lat1 = currentLoc[1];
	lng2 = Number(1*currentLoc[0] + 1*generateRandomLong()).toFixed(4);//1* is added to make sure the generated longitutde is a number and not a string
	lat2 = Number(1*currentLoc[1] + 1*generateRandomLat()).toFixed(4);//1* is added to make sure the generated latitude is a number and not a string
	distance = distcheck(lng1, lat1, lng2, lat2);
	//Bug alert 
	//The if function to check for the distance to be above 60 hasn't worked out yet
	if (distance < 60){
		lng2 = 1*currentLoc[0] + 1*generateRandomLong();
	    lat2 = 1*currentLoc[1] + 1*generateRandomLat();
		distance = distcheck(lng1, lat1, lng2, lat2);
    } 
    else{
		newRand = [lng2,lat2];
	}
    console.log('Current Location: ' + currentLoc);
    console.log('Random Location: ' + newRand);
    
    document.getElementById('distance').innerHTML = 'Estimated Distance: ' + Math.round(distance) + 'm';

    if (newMarker != undefined){
        newMarker.remove(map);
    }
    
    newMarker = new mapboxgl.Marker({color:'purple'})
        .setLngLat(newRand)
        .addTo(map);

    document.getElementById('showPath').disabled = false;
}

//Show path when the button is clicked
function showPath(){
    //Track starting location
    startLoc = currentLoc;
    console.log('Start Destination: ' + startLoc);
    //Track end location
    endLoc = newRand;
    console.log('Run Destination: ' + endLoc);
    
    map.addSource('route', {
    'type': 'geojson',
    'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates':[
                startLoc,
                endLoc
       ]
     }
  }
});
    
    map.addLayer({
    'id': 'route',
    'type': 'line',
    'source': 'route',
    'layout': {
        'line-join': 'round',
        'line-cap': 'round'
    },
    'paint': {
        'line-color': '#888',
        'line-width': 8
    }
});
    
document.getElementById('beginRun').disabled = false;
}

//Initiate the following actions when 'Begin Run' is pressed
function beginRun(){
    //Track the route taken by the user every second
    function addIn(){
        immediateLocations.push(currentLoc);
        console.log(immediateLocations);
        //Check the distance on real time from the destination
        realTimeDistance = distcheck(longitude, latitude, endLoc[0], endLoc[1]);
        console.log('Distance from Destination: ' + realTimeDistance);
    }
    //Call the function 'addIn' to run every second 
	intervalLoc = setInterval(addIn,1000);
    //Track the start time
    startTime = new Date();
    //Save new this new run instance in a class along with some starting infos
    runInstance = new Run();
    runInstance.setStartLoc(startLoc);
    runInstance.setEndLoc(endLoc);
    runInstance.setStartTime(startTime);
}

//Initiate the following actions when the user is 10m from the end location
//End run button is added because the accuracy function doesn't work well sometimes 
function endRun(){
    //Stop tracking location
    clearInterval(intervalLoc);
    console.log(immediateLocations);
    //Track the end time
    endTime = new Date();
    timeTaken = endTime.getTime() - startTime.getTime();
    console.log('Time Taken: ' + timeTaken/1000 + ' seconds');
    //Display time taken and distance travelled on the main screen
    document.getElementById('timeTaken').innerHTML = 'Time Taken: ' + timeTaken/1000 + ' seconds';
    distanceLeft = distcheck(startLoc[0], startLoc[1], endLoc[0], endLoc[1])
    console.log('Distance Left: ' + Math.round(distanceLeft));
    document.getElementById('distanceLeft').innerHTML = 'Distance Left: ' + Math.round(distanceLeft) + ' m';
    //Save more infos into the class
    runInstance.setPathTaken(immediateLocations);
    runInstance.setEndTime(endTime);
    runInstance.setTimeTaken(timeTaken);
    //Enable Save Run button
    document.getElementById("saveRun").disabled = false; 
}

//Save the run when the user presses the button
function saveRun(){
    runName = prompt("Please give a name for this run","");
    runInstance.setRunName(runName);
    console.log("Run instance : " + runInstance);
    //Check to see if local storage is present
    if (typeof(Storage) !== "undefined") 
    {
        //Check to see if the array is empty
        if (localStorage.getItem("runKey") !== null) 
        {
            let retrievedRun = localStorage.getItem("runKey"); //Retrieve key from local storage
            let runObject = JSON.parse(retrievedRun); //Parse into a new variable
            runObject.push(runInstance);
            localStorage.setItem("runKey",JSON.stringify(runObject)); //Save into local storage
        } 
        else {
            savedRuns.push(runInstance);
            let stringifiedRun = JSON.stringify(savedRuns); //Convert object into a JSON string
            localStorage.setItem("runKey",stringifiedRun); //Save into local storage
        }
    }
    else {
        console.log("Error: localStorage is not supported by current browser.");
    }
    
    location.href = 'index.html'; //Brings the user back to index page
    //Now clear memory.
    runInstance = null;
}