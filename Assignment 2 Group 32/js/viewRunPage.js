// Code for the View Run page.
var runNames = [];
var startLocs = [];
var endLocs = [];
var paths = [];
var timeTakens = [];
var runIndex = localStorage.getItem("selectedRun");
if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    let retrieveFromLocal = JSON.parse(localStorage.getItem("runKey"));
    for (i = 0; i < retrieveFromLocal.length; i++)
    {
        runNames.push(retrieveFromLocal[i]._runName);
        startLocs.push(retrieveFromLocal[i]._startLoc);
        endLocs.push(retrieveFromLocal[i]._endLoc);
        paths.push(retrieveFromLocal[i]._pathTaken);
        timeTakens.push(retrieveFromLocal[i]._timeTaken);
    
    }
    document.getElementById("headerBarTitle").textContent = runNames[runIndex]; //Display the title on view run page
    document.getElementById("time").innerHTML = "Time Taken: " + 1*timeTakens[runIndex]/1000 + "s";

}
//Mapbox access key
mapboxgl.accessToken = 'pk.eyJ1IjoidGVycmFuY2UwMCIsImEiOiJjazg4N3hiNGcwMG80M21teno1OXZseWxyIn0.9wSAxC4E13m9js90fUkBZg';

//Display map
let map = new mapboxgl.Map({
    container: 'map', //container id
    style: 'mapbox://styles/mapbox/streets-v11',
    center: startLocs[runIndex], //starting position
    zoom: 16 //starting zoom
});

//Display the starting position
marker = new mapboxgl.Marker()
        .setLngLat(startLocs[runIndex])
        .addTo(map);

//Display the destination location
newMarker = new mapboxgl.Marker({color:'purple'})
        .setLngLat(endLocs[runIndex])
        .addTo(map);

//Delete the run from history
function Delete() {
    var confirm = confirm("Are you sure to delete this run?");
    if (confirm) {
        retrieveFromLocal = JSON.parse(localStorage.getItem("runKey")); //Retrieve and parse key from local storage
        retrieveFromLocal.splice(runIndex, 1); //Remove the run 
        localStorage.setItem("runKey", JSON.stringify(retrieveFromLocal)); //Save into local storage
        alert('The route has been deleted!');
        location.href = 'index.html';
    } else {
        alert("No changes have been made.");
    }

}

//Repeat the run 
function Repeat() {
    retrieveFromLocal = JSON.parse(localStorage.getItem("runKey"))
    localStorage.setItem('repeatIndex', JSON.stringify(retrieveFromLocal[runIndex]));
    location.href = 'newRun.html';
}