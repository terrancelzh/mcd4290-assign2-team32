// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var runKey = "monash.mcd4290.runChallengeApp";
// Array of saved Run objects.
var savedRuns = [];
// Run Class
class Run 
{
    // Constructor to initialize the attributes
    constructor(startLoc, endLoc, pathTaken, startTime, endTime, timeTaken, runName) {
        //Private attributes
        this._startLoc = startLoc;
        this._endLoc = endLoc;
        this._pathTaken = pathTaken;
        this._startTime = startTime;
        this._endTime = endTime;
        this._timeTaken = timeTaken;
        this._runName = runName;
    }
    
    //Methods:
    //Mutator and accessor for starting location
    setStartLoc (startLoc)
    {
        this._startLoc = startLoc;
    }
    getStartLoc ()
    {
        return this._startLoc;
    }
    
    //Mutator and accessor for end location
    setEndLoc (endLoc)
    {
       this._endLoc = endLoc;
    }
    getEndLoc ()
    {
        return this._endLoc;
    }
    
    //Mutator and accessor for path taken
    setPathTaken(pathTaken)
    {
        this._pathTaken = pathTaken;
    }
    getPathTaken ()
    {
        return this._pathTaken;
    }
    
    //Mutator and accessor for starting time
    setStartTime(startTime)
    {
        this._startTime = startTime;
    }
    getStartTime ()
    {
        return this._startTime;
    }
    
    //Mutator and accessor for ending time
    setEndTime (endTime)
    {
       this._endTime = endTime; 
    }
    getEndTime ()
    {
        return this._endTime;
    }
    
    //Mutator and accessor for time taken
    setTimeTaken (timeTaken)
    {
        this._timeTaken = timeTaken;
    }
    getTimeTaken ()
    {
        return this._timeTaken;
    }
    
    //Mutator and accessor for run name
    setRunName (runName)
    {
        this._runName = runName;
    }
    getRunName ()
    {
        return this._runName;
    }
}